﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace ThumbnailGenerator
{
    public class ThumbnailManager
    {

        public async Task<string> GetThumbnailByVideo(string videoFullPath, string destPath, string videoId, int second)
        {
            // Generate thumbnail image from video with ffmpeg:
            // ffmpeg -ss 20 -i meinFilm.mp4 -t 0.001 -s 320x240 pic.jpg

            try
            {
                string tmpPathToFfmpeg = "ffmpeg";
                string tmpThumbnailFullPath = destPath + videoId + ".jpg";
                string tmpParameters = string.Format("-ss {0} -i \"{1}\" -s 320x240 -vframes 1 -y \"{2}\"", second, videoFullPath, tmpThumbnailFullPath);

                // Set up process
                var processInfo = new ProcessStartInfo();
                processInfo.FileName = tmpPathToFfmpeg;
                processInfo.Arguments = tmpParameters;
                processInfo.CreateNoWindow = true;
                processInfo.UseShellExecute = false;

                var process = new Process();
                process.StartInfo = processInfo;
                //process.WaitForExit();

                // Delete existing file
                File.Delete(tmpThumbnailFullPath);

                // Start process async
                await Task.Run(() => process.Start());

                return tmpThumbnailFullPath;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }

        }


    }

}
